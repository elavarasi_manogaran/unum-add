#include <stdlib.h>
#include "unum_add_test.h"
#include "libunum.h"

int ubound_add(ubound_t *result,ubound_t *x,ubound_t *y)
{
   ubound_t result_ubound,left_ubound,right_ubound;
   float left_bound,right_bound,left_bound_x,right_bound_x,left_bound_y,right_bound_y;

//If signs are same perform addition
    if(x->left_bound.sign	==	y->left_bound.sign)
       {
         //If both x & y are exact, perform exact addition
           // if((!x->InexactFlag)	&&	(!y->InexactFlag)){
           if((!x->uflag)	&&	(!y->uflag) && (!x->vflag)	&&	(!y->vflag)){
            // If x's left and right bounds are the same, then perform only left bound addition and copy the result to right bound
                if(unum_compare(x->left_bound,x->right_bound) && unum_compare(y->left_bound,y->right_bound)){
                         ubound_addExact_left_bound(&result_ubound,x,y);
                         result->left_bound = result_ubound.left_bound;
                         result->right_bound = result_ubound.right_bound;
                         left_bound = ub2f(&result_ubound);
                         right_bound = ub2f(&result_ubound);
                    }
           // If x's left and right bounds are different: this can happen while adding unum pairs with unums: add left and right bounds separately as exact numbers
                else{
                         ubound_addExact_left_bound(&left_ubound,x,y);
                         ubound_addExact_right_bound(&right_ubound,x,y);
                         result->left_bound = left_ubound.left_bound;
                         result->right_bound = right_ubound.right_bound;
                         left_bound	=	ub2f_left(&left_ubound);
                         right_bound =	ub2f_right(&right_ubound);
                    }
            }


          //Inexact addition

          else{
           //  printf("\n inexact addition");
             if((!x->uflag)	&&	(!y->uflag)) {

                     ubound_addExact_left_bound(&left_ubound,x,y);
                     ubound_addInexact_right_bound(&right_ubound,x,y);
                    }
              else if((!x->vflag)	&&	(!y->vflag)) {

                     ubound_addInexact_left_bound(&left_ubound,x,y);
                     ubound_addExact_right_bound(&right_ubound,x,y);
                    }
            else{
             ubound_addInexact_left_bound(&left_ubound,x,y);
             ubound_addInexact_right_bound(&right_ubound,x,y);
            }
             result->left_bound = left_ubound.left_bound;
             result->right_bound = right_ubound.right_bound;
             left_bound	=	ub2f_left(&left_ubound);
             right_bound =	ub2f_right(&right_ubound);

             }
       }

       //If signs are different perform subtraction
        else{

          //Exact subtraction
         //  if((!x->InexactFlag)	&&	(!y->InexactFlag)){
           if((!x->uflag)	&&	(!y->uflag) && (!x->vflag)	&&	(!y->vflag)){
                if(unum_compare(x->left_bound,x->right_bound) && unum_compare(y->left_bound,y->right_bound)){
                 ubound_subExact_left_bound(&result_ubound,x,y);
                 result->left_bound = result_ubound.left_bound;
                 result->right_bound = result_ubound.right_bound;
                 left_bound = ub2f(&result_ubound);
                 right_bound = ub2f(&result_ubound);
                }
            // If x's left and right bounds are different: this can happen while adding unum pairs with unums: add left and right bounds separately as exact numbers

                else{
                         ubound_subExact_left_bound(&left_ubound,x,y);
                         ubound_subExact_right_bound(&right_ubound,x,y);
                         result->left_bound = left_ubound.left_bound;
                         result->right_bound = right_ubound.right_bound;
                         left_bound	=	ub2f_left(&left_ubound);
                         right_bound =	ub2f_right(&right_ubound);
                    }
           }
            //Inexact subtraction
          else{

             if((!x->uflag)	&&	(!y->uflag)) {

                     ubound_subExact_left_bound(&left_ubound,x,y);
                     ubound_subInexact_right_bound(&right_ubound,x,y);
                    }
              else if((!x->vflag)	&&	(!y->vflag)) {

                     ubound_subInexact_left_bound(&left_ubound,x,y);
                     ubound_subExact_right_bound(&right_ubound,x,y);
                    }
              else{

             ubound_subInexact_left_bound(&left_ubound,x,y);
             ubound_subInexact_right_bound(&right_ubound,x,y);
            }
             result->left_bound = left_ubound.left_bound;
             result->right_bound = right_ubound.right_bound;
             left_bound	=	ub2f_left(&left_ubound);
             right_bound =	ub2f_right(&right_ubound);
              }
        }

return 0;
}

