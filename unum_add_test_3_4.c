/*Tests for unum addition (3,4)*/
#include <stdlib.h>
#include "unum_add_test.h"
#include "libunum.h"

void test_unum_add_3_4() {

    unum_t unum_x, unum_y;
    ubound_t op1,op2,result;
	double left_bound,right_bound;
	bool nan =0,left_open, right_open;

    set_env(3, 4);

    //0.025-200
    unum_x=	(unum_t)	{0,1,39321,1,3,15}	;	unum_y=	(unum_t)	{1,14,9,0,3,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-199.9765625	)&&(right_bound==	-199.974609375	));

    //0.0025-200
    unum_x=	(unum_t)	{0,6,18350,1,4,15}	;	unum_y=	(unum_t)	{1,14,9,0,3,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-199.998046875	)&&(right_bound==	-199.99609375	));

    //0.00025 - 200
    unum_x=	(unum_t)    {0,3,1572,1,4,15}   ;   unum_y=	(unum_t)	{1,14,9,0,3,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-200	)&&(right_bound==	-199.998046875	));

    //10.5 - 10.5
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{1,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	0	)&&(right_bound==	0	));

    //10.5 - 0.0001
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{1,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		assert((left_bound==	10.4998779296875	)&&(right_bound==	10.5	));

    //10.5 - 30
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{1,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-19.5	)&&(right_bound==	-19.5	));

    //10.5 - 0.00390625
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{1,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.49609375	)&&(right_bound==	10.49609375	));

    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{1,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-989.5	)&&(right_bound==	-989.5	));

    //10.5-0.00000000000000001256
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{1,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.4998779296875	)&&(right_bound==	10.5	));

    //4-0.00000000000000001256
    unum_x=	(unum_t)	{0,3,0,0,1,0}	;	unum_y=	(unum_t)	{1,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	3.999969482421875	)&&(right_bound==	4	));


    //-10.5+0.00000000000000001256
    unum_x=	(unum_t)	{1,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-10.5	)&&(right_bound==	-10.4998779296875	));
     //-0.00000000000000001256+10.5
    unum_x=	(unum_t)	{1,6,53089,1,6,15}	;	unum_y=	(unum_t) {0,6,5,0,2,3}		;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.4998779296875	)&&(right_bound==	10.5	));

    unum_x=	(unum_t)	{1,6,53089,1,6,15}	;	unum_y=	(unum_t) {0,6,0,0,2,0}		;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		//	assert((left_bound==	10.4998779296875	)&&(right_bound==	10.5	));


    //8-0.0001
    unum_x=	(unum_t)	{0,6,0,0,2,0}	;	unum_y=	(unum_t)	{1,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	7.9998779296875	)&&(right_bound==	7.99993896484375	));

    //-0.0001+8
      unum_x=	(unum_t)	{1,1,41838,1,4,15}	;	unum_y=	(unum_t) {0,6,0,0,2,0}		;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	7.9998779296875	)&&(right_bound==	7.99993896484375	));


    //10.5-0.0001256
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{1,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.499755859375	)&&(right_bound==	10.4998779296875	));
    //10.5-343
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{1,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-332.5	)&&(right_bound==	-332.5	));
    //10.5-1000
    unum_x=	(unum_t)	{1,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	989.5	)&&(right_bound==	989.50000000000000000000	));
    //0.0001-0.00390625
    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{1,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-0.0038062632083892822265625	)&&(right_bound==	-0.00380623340606689453125	));
    //0.0001256- 37.037037037
    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{1,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-37.037109375	)&&(right_bound==	-37.0361328125	));

    //0.56-0.58
    unum_x=	(unum_t)	{0,2,7864,1,2,15}	;	unum_y=	(unum_t)	{1,2,10485,1,2,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-0.0200042724609375	)&&(right_bound==	-0.019989013671875	));

    unum_x=	(unum_t)	{0,2,7864,1,2,15}	;	unum_y=	(unum_t)	{1,2,10485,1,2,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	-0.0200042724609375	)&&(right_bound==	-0.019989013671875	));
    //10.000001 -1.0002
    unum_x= (unum_t)	{0,6,16384,1,2,15}	;   unum_y=(unum_t)	{1,1,13,1,1,15}	; unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	8.999755859375	)&&(right_bound==	9	));

    //1000.00000000005 - 0.0002
    unum_x= (unum_t)	{0,24,62464,1,4,15}	;  unum_y= (unum_t)	{1,2,41838,1,4,15}	; unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	999.9921875	)&&(right_bound==	1000.0078125	));



    //Addition
    unum_x=	(unum_t)	{0,1,39321,1,3,15}	;	unum_y=	(unum_t)	{0,14,9,0,3,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		//	assert((left_bound==	0	)&&(right_bound==	0	));

    unum_x=	(unum_t)	{0,6,18350,1,4,15}	;	unum_y=	(unum_t)	{0,14,9,0,3,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		//	assert((left_bound==	0	)&&(right_bound==	0	));
    unum_x=	(unum_t)    {0,3,1572,1,4,15}   ;   unum_y=	(unum_t)	{0,14,9,0,3,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		//	assert((left_bound==	0	)&&(right_bound==	0	));

    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);				ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	21.00000000000000000000	)&&(right_bound==	21.00000000000000000000	));

    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.50000000000000000000	)&&(right_bound==	10.50012207031250000000	));
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	40.50000000000000000000	)&&(right_bound==	40.50000000000000000000	));
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.50390625000000000000	)&&(right_bound==	10.50390625000000000000	));
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1010.50000000000000000000	)&&(right_bound==	1010.50000000000000000000	));
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.50000000000000000000	)&&(right_bound==	10.50012207031250000000	));
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.50012207031250000000	)&&(right_bound==	10.50024414062500000000	));
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	353.50000000000000000000	)&&(right_bound==	353.50000000000000000000	));
    unum_x=	(unum_t)	{0,6,5,0,2,3}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	47.53662109375000000000	)&&(right_bound==	47.53710937500000000000	));
    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.50000000000000000000	)&&(right_bound==	10.50012207031250000000	));
    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		//	assert((left_bound==	0.00019999966025352400	)&&(right_bound==	0.00020000152289867400	));

    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	30.00000000000000000000	)&&(right_bound==	30.00024414062500000000	));

    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00400620698928833000	)&&(right_bound==	0.00400626659393310000	));

    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1000.00000000000000000000	)&&(right_bound==	1000.00781250000000000000	));
    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00009999983012676230	)&&(right_bound==	0.00010000169277191100	));

    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00022559799253940500	)&&(right_bound==	0.00022560171782970400	));

    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	343.00000000000000000000	)&&(right_bound==	343.00390625000000000000	));
    unum_x=	(unum_t)	{0,1,41838,1,4,15}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	37.03662109375000000000	)&&(right_bound==	37.03759765625000000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	40.50000000000000000000	)&&(right_bound==	40.50000000000000000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	30.00000000000000000000	)&&(right_bound==	30.00024414062500000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	60.00000000000000000000	)&&(right_bound==	60.00000000000000000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	30.00390625000000000000	)&&(right_bound==	30.00390625000000000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1030.00000000000000000000	)&&(right_bound==	1030.00000000000000000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,0,0,1,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	30.00000000000000000000	)&&(right_bound==	30.00000000000000000000	));

    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	30.00000000000000000000	)&&(right_bound==	30.00024414062500000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	30.00000000000000000000	)&&(right_bound==	30.00024414062500000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	373.00000000000000000000	)&&(right_bound==	373.00000000000000000000	));
    unum_x=	(unum_t)	{0,7,7,0,2,2}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	67.03613281250000000000	)&&(right_bound==	67.03710937500000000000	));
    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.50390625000000000000	)&&(right_bound==	10.50390625000000000000	));
    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00400620698928833000	)&&(right_bound==	0.00400626659393310000	));

    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	30.00390625000000000000	)&&(right_bound==	30.00390625000000000000	));
    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	0.00781250000000000000	)&&(right_bound==	0.00781250000000000000	));
    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1000.00000000000000000000	)&&(right_bound==	1000.00781250000000000000	));
    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		//	assert((left_bound==	0.00390625000000000000	)&&(right_bound==	0.00390630960464477000	));
    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		//	assert((left_bound==	0.00403183698654174000	)&&(right_bound==	0.00403189659118652000	));
    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	343.00390625000000000000	)&&(right_bound==	343.00390625000000000000	));
    unum_x=	(unum_t)	{0,7,0,0,4,0}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	37.04052734375000000000	)&&(right_bound==	37.04101562500000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1010.50000000000000000000	)&&(right_bound==	1010.50000000000000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1000.00000000000000000000	)&&(right_bound==	1000.00781250000000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1030.00000000000000000000	)&&(right_bound==	1030.00000000000000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1000.00000000000000000000	)&&(right_bound==	1000.00781250000000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	2000.00000000000000000000	)&&(right_bound==	2000.00000000000000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1000.00000000000000000000	)&&(right_bound==	1000.00781250000000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1000.00000000000000000000	)&&(right_bound==	1000.00781250000000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1343.00000000000000000000	)&&(right_bound==	1343.00000000000000000000	));
    unum_x=	(unum_t)	{0,24,61,0,4,5}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1037.03125000000000000000	)&&(right_bound==	1037.04687500000000000000	));
    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.50000000000000000000	)&&(right_bound==	10.50012207031250000000	));
    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00009999983012676230	)&&(right_bound==	0.00010000169277191100	));
    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	30.00000000000000000000	)&&(right_bound==	30.00024414062500000000	));
    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00390625000000000000	)&&(right_bound==	0.00390630960464477000	));
    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1000.00000000000000000000	)&&(right_bound==	1000.00781250000000000000	));
    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	 2.5119820842010344430*10^-17	)&&(right_bound==	2.5120032600247158006*10^-17	));
    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);		//	assert((left_bound==	0.00012559816241264300	)&&(right_bound==	0.00012560188770294100	));

    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	343.00000000000000000000	)&&(right_bound==	343.00390625000000000000	));
    unum_x=	(unum_t)	{0,6,53089,1,6,15}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	37.03662109375000000000	)&&(right_bound==	37.03759765625000000000	));
    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	10.50012207031250000000	)&&(right_bound==	10.50024414062500000000	));
    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00022559799253940500	)&&(right_bound==	0.00022560171782970400	));

    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	30.00000000000000000000	)&&(right_bound==	30.00024414062500000000	));
    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00403183698654174000	)&&(right_bound==	0.00403189659118652000	));
    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1000.00000000000000000000	)&&(right_bound==	1000.00781250000000000000	));
    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,0,0,1,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00012559816241264300	)&&(right_bound==	0.00012560002505779200	));
    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00012559816241264300	)&&(right_bound==	0.00012560188770294100	));

    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			//assert((left_bound==	0.00025119632482528600	)&&(right_bound==	0.00025120005011558500	));

    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	343.00000000000000000000	)&&(right_bound==	343.00390625000000000000	));
    unum_x=	(unum_t)	{0,2,1894,1,4,15}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	37.03662109375000000000	)&&(right_bound==	37.03759765625000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	353.50000000000000000000	)&&(right_bound==	353.50000000000000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	343.00000000000000000000	)&&(right_bound==	343.00390625000000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	373.00000000000000000000	)&&(right_bound==	373.00000000000000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	343.00390625000000000000	)&&(right_bound==	343.00390625000000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1343.00000000000000000000	)&&(right_bound==	1343.00000000000000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	343.00000000000000000000	)&&(right_bound==	343.00390625000000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	343.00000000000000000000	)&&(right_bound==	343.00390625000000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	686.00000000000000000000	)&&(right_bound==	686.00000000000000000000	));
    unum_x=	(unum_t)	{0,15,87,0,3,7}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	380.03515625000000000000	)&&(right_bound==	380.03906250000000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,6,5,0,2,3}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	47.53662109375000000000	)&&(right_bound==	47.53710937500000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,1,41838,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	37.03662109375000000000	)&&(right_bound==	37.03759765625000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,7,7,0,2,2}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	67.03613281250000000000	)&&(right_bound==	67.03710937500000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,7,0,0,4,0}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	37.04052734375000000000	)&&(right_bound==	37.04101562500000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,24,61,0,4,5}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	1037.03125000000000000000	)&&(right_bound==	1037.04687500000000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,6,53089,1,6,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	37.03662109375000000000	)&&(right_bound==	37.03759765625000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,2,1894,1,4,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	37.03662109375000000000	)&&(right_bound==	37.03759765625000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,15,87,0,3,7}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	380.03515625000000000000	)&&(right_bound==	380.03906250000000000000	));
    unum_x=	(unum_t)	{0,12,10315,1,3,15}	;	unum_y=	(unum_t)	{0,12,10315,1,3,15}	;	unum2ubound(&op1,&unum_x);				unum2ubound(&op2,&unum_y);			ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);			assert((left_bound==	74.07324218750000000000	)&&(right_bound==	74.07421875000000000000	));

/*** Testcases
//Testcase 2

x= (unum_t)	{0,2,7864,1,2,15}	; //0.56
y= (unum_t)	{0,2,10485,1,2,15}	; //0.58
z= (unum_t)	{0,2,7864,1,2,15}	; //0.56


//Testcase 3

x= (unum_t)	{0,2,7864,1,2,15}	; //0.56
y= (unum_t)	{0,2,7864,1,2,15}	; //0.56
z= (unum_t)	{0,2,7864,1,2,15}	; //0.56

//Testcase 5


x= (unum_t)	{0,6,24616,1,2,15}	; //11.005
y= (unum_t)	{0,1,3276,1,1,15}	; //1.05
z= (unum_t)	{0,2,65509,1,2,15}	; //0.998


// (10.5 + 0.0001)
x= (unum_t)	{0,6,5,0,2,3}	;y=(unum_t)	{0,1,41838,1,4,15}	;unum2ubound(&x_ub,	x);	unum2ubound(&y_ub,	y); if((x).sign	==	(y).sign){if((!(x).ubit)	&&	((!(y).ubit)))	{unum_add(&result_ubound,&x_ub,	&y_ub);	left_bound	=	ub2f_left(&result_ubound);	right_bound	=	ub2f_right(&result_ubound);	}	else	{	unum_add_left_bound(&left_ubound,&x_ub,	&y_ub);	unum_add_right_bound(&right_ubound,&x_ub,	&y_ub);	left_bound_left	=	ub2f_left(&left_ubound);	left_bound_right	=	ub2f_right(&left_ubound);	right_bound_left	=	ub2f_left(&right_ubound);	right_bound_right	=	ub2f_right(&right_ubound);	left_bound	=	ub2f_left(&left_ubound);	right_bound	=	ub2f_right(&right_ubound);}if(right_bound<left_bound)	swap_bounds(&left_bound,&right_bound);			assert((left_bound==		10.5	)&&(right_bound==		10.5001220703125	));}

//(1/256) + 30
x= (unum_t)	{0,7,0,0,4,0}	;y=(unum_t)	{0,7,7,0,2,2}	;unum2ubound(&x_ub,	x);	unum2ubound(&y_ub,	y); if((x).sign	==	(y).sign){if((!(x).ubit)	&&	((!(y).ubit)))	{unum_add(&result_ubound,&x_ub,	&y_ub);	left_bound	=	ub2f_left(&result_ubound);	right_bound	=	ub2f_right(&result_ubound);	}	else	{	unum_add_left_bound(&left_ubound,&x_ub,	&y_ub);	unum_add_right_bound(&right_ubound,&x_ub,	&y_ub);	left_bound_left	=	ub2f_left(&left_ubound);	left_bound_right	=	ub2f_right(&left_ubound);	right_bound_left	=	ub2f_left(&right_ubound);	right_bound_right	=	ub2f_right(&right_ubound);	left_bound	=	ub2f_left(&left_ubound);	right_bound	=	ub2f_right(&right_ubound);}if(right_bound<left_bound)	swap_bounds(&left_bound,&right_bound);			assert((left_bound==		30.00390625	)&&(right_bound==		30.00390625	));}

//(1/256) + 1000
x= (unum_t)	{0,7,0,0,4,0}	;y=(unum_t)	{0,24,61,0,4,5}	;unum2ubound(&x_ub,	x);	unum2ubound(&y_ub,	y); if((x).sign	==	(y).sign){if((!(x).ubit)	&&	((!(y).ubit)))	{unum_add(&result_ubound,&x_ub,	&y_ub);	left_bound	=	ub2f_left(&result_ubound);	right_bound	=	ub2f_right(&result_ubound);	}	else	{	unum_add_left_bound(&left_ubound,&x_ub,	&y_ub);	unum_add_right_bound(&right_ubound,&x_ub,	&y_ub);	left_bound_left	=	ub2f_left(&left_ubound);	left_bound_right	=	ub2f_right(&left_ubound);	right_bound_left	=	ub2f_left(&right_ubound);	right_bound_right	=	ub2f_right(&right_ubound);	left_bound	=	ub2f_left(&left_ubound);	right_bound	=	ub2f_right(&right_ubound);}if(right_bound<left_bound)	swap_bounds(&left_bound,&right_bound);			assert((left_bound==		1000	)&&(right_bound==		1000.0078125	));}

//(0,0.00006103515625) + (0,0.00006103515625)
x= (unum_t)	{0,0,0,1,3,7}	;y=(unum_t)	{0,0,0,1,3,7}	;unum2ubound(&x_ub,	x);	unum2ubound(&y_ub,	y); if((x).sign	==	(y).sign){if((!(x).ubit)	&&	((!(y).ubit)))	{unum_add(&result_ubound,&x_ub,	&y_ub);	left_bound	=	ub2f_left(&result_ubound);	right_bound	=	ub2f_right(&result_ubound);	}	else	{	unum_add_left_bound(&left_ubound,&x_ub,	&y_ub);	unum_add_right_bound(&right_ubound,&x_ub,	&y_ub);	left_bound_left	=	ub2f_left(&left_ubound);	left_bound_right	=	ub2f_right(&left_ubound);	right_bound_left	=	ub2f_left(&right_ubound);	right_bound_right	=	ub2f_right(&right_ubound);	left_bound	=	ub2f_left(&left_ubound);	right_bound	=	ub2f_right(&right_ubound);}if(right_bound<left_bound)	swap_bounds(&left_bound,&right_bound);			assert((left_bound==		0	)&&(right_bound== 0.0001220703125));}

//(0.00000000000000001256 + 0.0001256)
//(0.0000000000000000125599104210051722152474695803903159685432910919189453125,0.000000000000000012560016300123579002789853120702900923788547515869140625) + (0.0001255981624126434326171875,0.00012560002505779266357421875)
x= (unum_t)	{0,6,53089,1,6,15}	;y=(unum_t)	{0,2,1894,1,4,15}	;unum2ubound(&x_ub,	x);	unum2ubound(&y_ub,	y); if((x).sign	==	(y).sign){if((!(x).ubit)	&&	((!(y).ubit)))	{unum_add(&result_ubound,&x_ub,	&y_ub);	left_bound	=	ub2f_left(&result_ubound);	right_bound	=	ub2f_right(&result_ubound);	}	else	{	unum_add_left_bound(&left_ubound,&x_ub,	&y_ub);	unum_add_right_bound(&right_ubound,&x_ub,	&y_ub);	left_bound_left	=	ub2f_left(&left_ubound);	left_bound_right	=	ub2f_right(&left_ubound);	right_bound_left	=	ub2f_left(&right_ubound);	right_bound_right	=	ub2f_right(&right_ubound);	left_bound	=	ub2f_left(&left_ubound);	right_bound	=	ub2f_right(&right_ubound);}if(right_bound<left_bound)	swap_bounds(&left_bound,&right_bound);			assert((left_bound==		0.0001255981624126434326171875	)&&(right_bound== 0.00012560188770294189453125));}

//343 + 37.037037037
x= (unum_t)	{0,15,87,0,3,7}	;y=(unum_t)	{0,12,10315,1,3,15}	;unum2ubound(&x_ub,	x);	unum2ubound(&y_ub,	y); if((x).sign	==	(y).sign){if((!(x).ubit)	&&	((!(y).ubit)))	{unum_add(&result_ubound,&x_ub,	&y_ub);	left_bound	=	ub2f_left(&result_ubound);	right_bound	=	ub2f_right(&result_ubound);	}	else	{	unum_add_left_bound(&left_ubound,&x_ub,	&y_ub);	unum_add_right_bound(&right_ubound,&x_ub,	&y_ub);	left_bound_left	=	ub2f_left(&left_ubound);	left_bound_right	=	ub2f_right(&left_ubound);	right_bound_left	=	ub2f_left(&right_ubound);	right_bound_right	=	ub2f_right(&right_ubound);	left_bound	=	ub2f_left(&left_ubound);	right_bound	=	ub2f_right(&right_ubound);}if(right_bound<left_bound)	swap_bounds(&left_bound,&right_bound);			assert((left_bound==		380.03515625	)&&(right_bound== 380.0390625));}

x= (unum_t)	{0,6,1,0,2,1}	;y=(unum_t)	{0,3,1,0,1,1}	;unum2ubound(&x_ub,	x);	unum2ubound(&y_ub,	y); if((x).sign	==	(y).sign){if((!(x).ubit)	&&	((!(y).ubit)))	{unum_add(&result_ubound,&x_ub,	&y_ub);	left_bound	=	ub2f_left(&result_ubound);	right_bound	=	ub2f_right(&result_ubound);	}	else	{	unum_add_left_bound(&left_ubound,&x_ub,	&y_ub);	unum_add_right_bound(&right_ubound,&x_ub,	&y_ub);	left_bound_left	=	ub2f_left(&left_ubound);	left_bound_right	=	ub2f_right(&left_ubound);	right_bound_left	=	ub2f_left(&right_ubound);	right_bound_right	=	ub2f_right(&right_ubound);	left_bound	=	ub2f_left(&left_ubound);	right_bound	=	ub2f_right(&right_ubound);}if(right_bound<left_bound)	swap_bounds(&left_bound,&right_bound);			assert((left_bound==		15	)&&(right_bound== 15));}

x= (unum_t)	{1,6,1,0,2,1}	;//-10
y=(unum_t)	{0,3,1,0,1,1}	;//5


x= (unum_t)	{0,6,24579,0,2,15}	; //11.0003---
z=(unum_t)	{0,2,65509,1,2,15}	; //0.9998
w=(unum_t)	{0,1,13,1,1,15}	; //1.0002

x= (unum_t)	{0,0,1,0,1,0}	; //0.5
y= (unum_t)	{0,3,0,0,1,0}	; //4
x= (unum_t) {0,3,0,1,1,1}   ; //(4,5)
y= (unum_t)	{0,1,0,0,0,0}	; //2

x= (unum_t)	{0,6,16384,1,2,15}	; //10.000001
y=(unum_t)	{1,1,13,1,1,15}	; //-1.0002

x= (unum_t)	{0,6,16384,1,2,15}	; //10.000001
y=(unum_t)	{1,5,22424,1,5,15}	; //-0.00000002

x= (unum_t)	{1,7,7,0,2,2}	; //30
y=(unum_t)	{0,7,0,0,4,0}	; //-0.00390625



unum_x= (unum_t)	{0,6,16384,1,2,15}	; //10.000001
unum_y=(unum_t)	{0,1,13,1,1,15}	; //1.0002
unum_z= (unum_t)	{0,2,65509,1,2,15}	; //0.998



unum_x= (unum_t)	{0,24,62464,1,4,15}	; //1000.00000000005
unum_y= (unum_t)	{0,2,41838,1,4,15}	; //0.0002
unum_z= (unum_t)	{0,2,41838,1,4,15}	; //0.0002


unum_x= (unum_t)	{0,24,62464,1,4,15}	; //1000.00000000005
unum_y= (unum_t)	{0,2,41838,1,4,15}	; //0.0002
unum_x= (unum_t)	{0,24,31232,1,4,14}	; //(1000, 1000.015625)
unum_y=(unum_t)	{0,1,13,1,1,15}	; //1.0002

***/
/*
   printf("\n unum add result is\n");
   printf("\n Left bound: %5.20f\n",left_bound);
   printf("\n");
   printu((result).left_bound);
   printf("\n Right bound: %5.20f\n",right_bound);
   printf("\n");
   printu((result).right_bound);
*/
   printf("\n All tests for 3,4 passed :)");

}
