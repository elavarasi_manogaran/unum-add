#include <stdlib.h>
#include "unum_add_test.h"
#include "libunum.h"

// A convenience function to add 2 unums and return the result as a unum after unification
int unum_add(ubound_t *result,unum_t *x,unum_t *y)
{
     int status;
     ubound_t op1,op2,uboundResult;

         status = unum2ubound(&op1, x);
         if (status) return(status); // Return error

         status = unum2ubound(&op2, y);
         if (status) return(status); // Return error

         status = ubound_add(&uboundResult, &op1, &op2);
         if (status) return(status); // Return error

         // User wants a unum result, so have to unify - yet to code for unification
         status = ubound_unify(&uboundResult, result);
         if (status) return(status);

       return(0); // All went well
}


