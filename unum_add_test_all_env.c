/*Tests for unum.h*/
#include "unum_add_test.h"
#include "libunum.h"

void test_unum_add_all_envs() {

    test_unum_add_0_0();
	test_unum_add_0_1();
	test_unum_add_1_0();
    test_unum_add_1_1();
    test_unum_add_3_4();
}
