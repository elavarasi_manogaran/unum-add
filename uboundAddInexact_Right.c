/***Unum Inexact addition - Right ubound***/
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "libunum.h"


void ubound_addInexact_right_bound(ubound_t *result_ub, ubound_t *x_ub,  ubound_t *y_ub ) {


    unsigned short trailing_zeros, fraction_bit_length_op1_num, fraction_bit_length_op2_num;
    short bias_of_op1, bias_of_op2,exponent_of_op1,exponent_of_op2,exponent_of_result,exponent_of_result_m1;
    unsigned int fraction_bit_length,max_of_oprnd_fraction_bit_length,added_fractions_before, LSB_bits2Scrapeoff;
    int expDiff,abs_em1_int,scale_to_be_added_to_exponent;
    unsigned long long op1_num, op2_num,added_fractions;

 // operands are of the same sign in addition
    result_ub->right_bound.sign =  x_ub->right_bound.sign;
    result_ub->uflag = 1;
     result_ub->vflag = 1;


// Check if the left bounds are NAN: set result to NAN
    if(isNaN(x_ub->right_bound) || isNaN(y_ub->right_bound))
        {

         if(x_ub->right_bound.sign | y_ub->right_bound.sign){
           result_ub->left_bound = _g_sNaNu;
            result_ub->right_bound = _g_sNaNu;
        }
        else{
        result_ub->left_bound = _g_qNaNu;
        result_ub->right_bound = _g_qNaNu;
        }

        return;
        }


 // Check if operands are +INF and -INF: set result to NAN

    if(isInf(x_ub->right_bound) && isInf(y_ub->right_bound))
    {
        if(x_ub->right_bound.sign ^ y_ub->right_bound.sign)
        {
           result_ub->left_bound = _g_qNaNu;
           result_ub->right_bound = _g_qNaNu;
           return;
        }

    }


//posinf or neginf
    if(isPosInf(x_ub->right_bound)){
       if(!x_ub->InexactFlag)
        {
           //if(isPosInf(x_ub->left_bound)){
               result_ub->left_bound = _g_posinfu;
               result_ub->right_bound = _g_posinfu;
                     }
     else
                {
                   result_ub->left_bound = _g_maxrealuopen;
                    result_ub->right_bound = _g_maxrealuopen;
                }
                return;
        }

    if(isPosInf(y_ub->right_bound)){
       if(!y_ub->InexactFlag)
        {
          // if(isPosInf(y_ub->left_bound)){
               result_ub->left_bound = _g_posinfu;
               result_ub->right_bound = _g_posinfu;
               }
           else
            {
                result_ub->left_bound = _g_maxrealuopen;
                result_ub->right_bound = _g_maxrealuopen;
            }
            return;
        }

    if(isNegInf(x_ub->right_bound))
        {
           if(!x_ub->InexactFlag){
               result_ub->left_bound = _g_neginfu;
               result_ub->right_bound = _g_neginfu;
               }
                else
                {
                   result_ub->left_bound = _g_minrealuopen;
                    result_ub->right_bound = _g_minrealuopen;
                }
                return;
        }

    if(isNegInf(y_ub->right_bound))
        {
            if(!y_ub->InexactFlag){
               result_ub->left_bound = _g_neginfu;
               result_ub->right_bound = _g_neginfu;
              }
               else
                {
                   result_ub->left_bound = _g_minrealuopen;
                    result_ub->right_bound = _g_minrealuopen;
                }
                return;
        }

    //If any one of the operands are positive or negative zero, then simply return the other operand as result
    if((isPosOrNegZeroExact(x_ub->right_bound)) && (!x_ub->InexactFlag)) {

          if(y_ub->right_bound.sign){
         result_ub->left_bound = y_ub->right_bound;
         result_ub->right_bound = y_ub->right_bound;
         result_ub->left_bound.ubit=1;
         result_ub->right_bound.ubit=1;

         }
         else{
          result_ub->left_bound = y_ub->left_bound;
          result_ub->right_bound = y_ub->left_bound;
          result_ub->left_bound.ubit=1;
          result_ub->right_bound.ubit=1;
         }
         return;
    }


    if((isPosOrNegZeroExact(y_ub->right_bound)) && (!y_ub->InexactFlag))
    {

        if(x_ub->right_bound.sign){
         result_ub->left_bound = x_ub->right_bound;
         result_ub->right_bound = x_ub->right_bound;
         result_ub->left_bound.ubit=1;
         result_ub->right_bound.ubit=1;

         }
         else{
          result_ub->left_bound = x_ub->left_bound;
          result_ub->right_bound = x_ub->left_bound;
          result_ub->left_bound.ubit=1;
          result_ub->right_bound.ubit=1;
         }
         return;
    }


// Calculating exponents and bias for normal numbers

 	bias_of_op1 = pow(2,(x_ub->right_bound).e_size) - 1;


 	if((x_ub->right_bound).exponent){
        exponent_of_op1 = (x_ub->right_bound).exponent - bias_of_op1;
        op1_num = (x_ub->right_bound).fraction + pow(2,((x_ub->right_bound).f_size + 1));
 	}

    else{
       exponent_of_op1 = 1- bias_of_op1;
        op1_num = (x_ub->right_bound).fraction;
    }

 //printf("\n op1_num =%d",op1_num);
    bias_of_op2 = pow(2,(y_ub->right_bound).e_size) - 1;

 	if((y_ub->right_bound).exponent){
        op2_num = (y_ub->right_bound).fraction + pow(2,((y_ub->right_bound).f_size + 1));
        exponent_of_op2 = (y_ub->right_bound).exponent - bias_of_op2;
 	}


 	 else{
        exponent_of_op2 = 1 - bias_of_op2;
        op2_num = (y_ub->right_bound).fraction;
 	 }


//calculate difference in exponent - result's left bound
    expDiff = exponent_of_op1 - exponent_of_op2;


    fraction_bit_length_op1_num = x_ub->right_bound.f_size + 1;
    fraction_bit_length_op2_num = y_ub->right_bound.f_size + 1;
    max_of_oprnd_fraction_bit_length = (fraction_bit_length_op2_num > fraction_bit_length_op1_num)? fraction_bit_length_op2_num : fraction_bit_length_op1_num;


         //Align the binary points if they are not aligned before addition
         //Align fraction bitlength
            if(fraction_bit_length_op2_num < fraction_bit_length_op1_num)
                    op2_num <<= (fraction_bit_length_op1_num - fraction_bit_length_op2_num);

            if(fraction_bit_length_op2_num > fraction_bit_length_op1_num)
                    op1_num <<= (fraction_bit_length_op2_num - fraction_bit_length_op1_num);



             // If both the exponents are the same
        if(!expDiff){

                added_fractions = op1_num + op2_num;

                fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                fraction_bit_length = (fraction_bit_length < max_of_oprnd_fraction_bit_length)? max_of_oprnd_fraction_bit_length: fraction_bit_length;

                //If the result is 0, then return zero as result
                if((!added_fractions))
                    {
                       if(result_ub->right_bound.sign)
                        result_ub->right_bound = _g_negzeroopen;
                     else
                        result_ub->right_bound = _g_zeroopen;
 // printf("\n fraction_bit_length  =%d",fraction_bit_length);

                    result_ub->right_bound.f_size =   fraction_bit_length - 1;
                    result_ub->right_bound.e_size = _g_esizemax - 1;

                       // result_ub->right_bound.ubit = 1;
                        result_ub->left_bound = result_ub->right_bound;
                     return;
                    }

                //If any one of the operand is normal
                else if((x_ub->right_bound).exponent || (y_ub->right_bound).exponent) {

                  scale_to_be_added_to_exponent = ((fraction_bit_length - (max_of_oprnd_fraction_bit_length + 1)));


                  exponent_of_result = exponent_of_op1 + scale_to_be_added_to_exponent;

                        }
                //If both the operands are subnormal (has zero as the leading bit) then the result gets the same exponent as the operands
                //Here the result's exponent cannot be generalized to zero since adding a subnormal 1 (with exponent one) and a subnormal 0.5 with exponent 0 will lead to a normal number with exponent 1
                else
                     exponent_of_result = exponent_of_op1;

        }
        //If the operands have different exponents
        else{
            //If the difference in exponent is negative, then normalize the exponent
        if(expDiff < 0){

              if(-expDiff > _g_fsizemax)
                        {
                        result_ub->right_bound.exponent =  y_ub->right_bound.exponent;
                        result_ub->right_bound.e_size = y_ub->right_bound.e_size;
                        added_fractions = y_ub->right_bound.fraction << (_g_fsizemax - fraction_bit_length_op2_num);
                        result_ub->right_bound.fraction = added_fractions;
                        result_ub->right_bound.f_size = _g_fsizemax - 1;
                        result_ub->right_bound.ubit = 1;
                         result_ub->left_bound = result_ub->right_bound;

                        if(isNaNOrInf(result_ub->right_bound))
                            {
                                if(!result_ub->right_bound.sign)
                                result_ub->right_bound = _g_maxrealuopen;

                              else
                                 result_ub->right_bound = _g_minrealuopen;

                                result_ub->left_bound = result_ub->right_bound;
                                return;
                            }

                        return;
                        }
                op2_num <<= -expDiff;
                added_fractions =  op1_num  + op2_num;
                fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                fraction_bit_length = (fraction_bit_length < max_of_oprnd_fraction_bit_length)? max_of_oprnd_fraction_bit_length: fraction_bit_length;

              //If the result is 0, then return zero as result
                if(!added_fractions)
                    {
                        if(result_ub->right_bound.sign)
                        result_ub->right_bound = _g_negzeroopen;
                        else
                        result_ub->right_bound = _g_zeroopen;

                        result_ub->right_bound.f_size =   fraction_bit_length - 1;
                        result_ub->right_bound.e_size = _g_esizemax - 1;
                        result_ub->left_bound = result_ub->right_bound;
                     return;
                    }

            //If any one of the operand is normal
                else if((x_ub->right_bound).exponent || (y_ub->right_bound).exponent) {

                         scale_to_be_added_to_exponent = fraction_bit_length - (max_of_oprnd_fraction_bit_length + 1 - expDiff);
                         exponent_of_result = exponent_of_op2 + scale_to_be_added_to_exponent;

                        }

                else
                 exponent_of_result = exponent_of_op1;


            }
       //If the difference in exponent is negative, then normalize the exponent
        else{

                // Adding a larger number with a smaller number   - left shift the fraction until it fits maximum fraction size and set the ubit
                 if(expDiff > _g_fsizemax)
                        {
                        result_ub->right_bound.exponent =  x_ub->right_bound.exponent;
                        result_ub->right_bound.e_size = x_ub->right_bound.e_size;
                        added_fractions = x_ub->right_bound.fraction <<  (_g_fsizemax - fraction_bit_length_op1_num);
                        result_ub->right_bound.fraction = added_fractions;
                        result_ub->right_bound.f_size = _g_fsizemax -1;
                        result_ub->right_bound.ubit = 1;

                //If the result lands in Infinity or NaN - handle them as special cases
                //If the result is exact and the pattern resembles infinity, then reduce one ulp from the result and set the ubit (in other words assign it to maxrealuopen or minrealuopen

                if(isNaNOrInf(result_ub->right_bound))
                {

                   if(!result_ub->right_bound.sign)
                    result_ub->right_bound = _g_maxrealuopen;

                  else
                     result_ub->right_bound = _g_minrealuopen;
                return;
                }


                result_ub->left_bound = result_ub->right_bound;
                return;
                        }



                op1_num <<= expDiff;
                added_fractions =  op1_num  + op2_num;

                fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                fraction_bit_length = (fraction_bit_length < max_of_oprnd_fraction_bit_length)? max_of_oprnd_fraction_bit_length: fraction_bit_length;
                //If the result is 0, then return zero as result
                if(!added_fractions)
                    {
                    if(result_ub->right_bound.sign)
                        result_ub->right_bound = _g_negzeroopen;
                     else
                        result_ub->right_bound = _g_zeroopen;


                    result_ub->right_bound.f_size =   fraction_bit_length - 1;
                    result_ub->right_bound.e_size = _g_esizemax - 1;
                        result_ub->left_bound = result_ub->right_bound;
                     return;
                    }

                else if((((x_ub->right_bound).exponent) || ((y_ub->right_bound).exponent)))
                  {
                     scale_to_be_added_to_exponent = fraction_bit_length - (max_of_oprnd_fraction_bit_length + 1 + expDiff);
                     exponent_of_result = exponent_of_op1 + scale_to_be_added_to_exponent;
                  }

                else
                    exponent_of_result =  exponent_of_op2;


        }
        }
        //result's left bound is greater than what can be expressed - example: 3 in {0,0} environment
                if ( exponent_of_result > _g_maxexp){



                 //If the result is negative, then set the left bound as minimum representable real value
                if(result_ub->right_bound.sign){

                        result_ub->right_bound = _g_minrealuopen;
                                            }
              //If the result is positive, then set the left bound as maximum representable real value
                else{
                   result_ub->right_bound = _g_maxrealuopen;
                    }

               //set the right bound equal to the left bound
                result_ub->left_bound = result_ub->right_bound;

                return;

                }

           //Calculate the esize - for both normal and subnormal
                exponent_of_result_m1 = exponent_of_result - 1;
                abs_em1_int = (unsigned int) abs(exponent_of_result_m1);
                if(abs_em1_int == 0)
                    result_ub->right_bound.e_size = 0;
                else
                    result_ub->right_bound.e_size = INTERMEDIATE_EXP_SIZE - (unsigned short) (__builtin_clz(abs_em1_int)-16);


                //If any one of the exponents are nonzero (normal number)
                if(((x_ub->right_bound).exponent) || ((y_ub->right_bound).exponent))
                    {
                        result_ub->right_bound.exponent = exponent_of_result_m1 + (1 << result_ub->right_bound.e_size);
                        //clip-off the hidden bit
                        added_fractions &= ~(1ULL << (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions) - 1));
                       //Reduce one bit for the hidden bit
                        fraction_bit_length--;

                       //If added fractions after clipping off hidden bit is zero, then set fraction to zero
                        if(!added_fractions)
                              {
                                  result_ub->right_bound.fraction = 0;
                                   if(fraction_bit_length > _g_fsizemax )
                                    fraction_bit_length = _g_fsizemax;

                             }


                       //If added fractions is nonzero, then clip off the trailing zeroes and check if the fraction bitlength after clipping off the trailing zeroes exceeds the maximum fraction size, if so set the ubit
                        else
                        {

                             trailing_zeros = __builtin_ctzll(added_fractions);

                        //right shift if fbl exceeds max size of operands - might loose bits here if the lsb is non-zero
                                    if(fraction_bit_length > _g_fsizemax )
                                        {
                                         if ((fraction_bit_length - trailing_zeros) <= _g_fsizemax) /* && (!result_ub->right_bound.ubit)) */{
                                                 result_ub->right_bound.ubit = 0;
                                                 result_ub->uflag = 0;
                                                 result_ub->vflag = 0;
                                                 added_fractions >>= trailing_zeros;
                                                 fraction_bit_length-=  trailing_zeros;
                                            }
                                        else {
                                        result_ub->right_bound.ubit = 1;
                                        added_fractions >>= fraction_bit_length - _g_fsizemax;

                                        fraction_bit_length = _g_fsizemax;
                                              }

                                             result_ub->right_bound.f_size = fraction_bit_length - 1;
                                             result_ub->right_bound.exponent = exponent_of_result_m1 + (1 << result_ub->right_bound.e_size);

                                            if(result_ub->right_bound.ubit)
                                                result_ub->right_bound.fraction = added_fractions;
                                            else{
                                                trailing_zeros = __builtin_ctzll(added_fractions);
                                                added_fractions >>= trailing_zeros;
                                                result_ub->right_bound.fraction = added_fractions;
                                                fraction_bit_length-=  trailing_zeros;
                                                }

                                        result_ub->right_bound.fraction = added_fractions;


//If the result lands in Infinity or NaN - handle them as special cases
//If the result is exact and the pattern resembles infinity, then reduce one ulp from the result and set the ubit (in other words assign it to maxrealuopen or minrealuopen

                if(isNaNOrInf(result_ub->right_bound))
                {

                   if(!result_ub->right_bound.sign)
                    result_ub->right_bound = _g_maxrealuopen;

                  else
                     result_ub->right_bound = _g_minrealuopen;
                return;
                }


                result_ub->left_bound = result_ub->right_bound;

               return;
                }

                }
            }

            //subnormal numbers
                else
                {

                 //If the exponents of both the operands are 0 then the result's exponent is zero
                        result_ub->right_bound.exponent = 0;


                exponent_of_result_m1 = exponent_of_result - 1;
                abs_em1_int = (unsigned int) abs(exponent_of_result_m1);
                if(abs_em1_int == 0)
                    result_ub->right_bound.e_size = 0;
                else
                    result_ub->right_bound.e_size = INTERMEDIATE_EXP_SIZE - (unsigned short) (__builtin_clz(abs_em1_int)-16);



                        if(fraction_bit_length > max_of_oprnd_fraction_bit_length){
                            added_fractions &= ~(1ULL << (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions) - 1));
                            result_ub->right_bound.fraction = added_fractions;
                            fraction_bit_length--;
                            result_ub->right_bound.exponent++;
                            //reset the ubit if fraction fits in the fraction field
                            result_ub->right_bound.ubit=0;
                            }

                }


            result_ub->right_bound.fraction = added_fractions;

            if((fraction_bit_length == 1) )
                result_ub->right_bound.f_size = 0;
            else
                result_ub->right_bound.f_size = fraction_bit_length - 1;
             result_ub->right_bound.ubit = 1;
            //set the right bound equal to the left bound
           result_ub->left_bound = result_ub->right_bound;



//If the result lands in Infinity or NaN - handle them as special cases
//If the result is exact and the pattern resembles infinity, then reduce one ulp from the result and set the ubit (in other words assign it to maxrealuopen or minrealuopen

                if(isNaNOrInf(result_ub->right_bound))
                {
                   if(!result_ub->right_bound.sign)
                    result_ub->right_bound = _g_maxrealuopen;

                  else
                     result_ub->right_bound = _g_minrealuopen;

                    result_ub->left_bound = result_ub->right_bound;
                    return;



                }

               //If the right bound sign is positive, then subtract 1 ULP to get the result
               if(!result_ub->right_bound.sign){
                //If the right bound fraction is 0, then subtract one from exponent and set the fraction to be the maximum representable fraction and fsize to be the maximum
                if (!result_ub->right_bound.fraction)
                        {
                            result_ub->right_bound.exponent-=1;
                            result_ub->right_bound.fraction= _g_maxfracval;
                            result_ub->right_bound.f_size =  _g_fsizemax-1;
                            result_ub->right_bound.ubit = 1;
                            result_ub->left_bound = result_ub->right_bound;
                            return;
                        }
                //If fraction could be reduced, reduce 1 ULP to get the right bound result
                else
                {  //only if the ubit is set, then reduce 1 from the right bound fraction

                   result_ub->right_bound.fraction-=1;
                   result_ub->right_bound.ubit = 1;
                   result_ub->left_bound = result_ub->right_bound;
                   return;

                }
               }

    return;
    }






