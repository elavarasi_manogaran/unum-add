/*
 * Header file for unum add test functions
 */
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
//#include <gmp.h>
#include "libunum.h"

#ifndef UNUM_ADD_TEST_H
#define UNUM_ADD_TEST_H

/*
 * Test the unum addition for all environments
 */
void test_unum_add_all_envs();

/*
 * Test the unum addition for environment {0,0}
 */
void test_unum_add_0_0();

/*
 * Test the unum addition for environment {0,1}
 */
void test_unum_add_0_1();

/*
 * Test the unum addition for environment {1,0}
 */
void test_unum_add_1_0();

/*
 * Test the unum addition for environment {1,1}
 */
void test_unum_add_1_1();

/*
 * Test the unum addition for environment {3,4}
 */
void test_unum_add_3_4();


#endif

