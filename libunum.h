#ifndef LIBUNUM_H
#define LIBUNUM_H
/*
 * Header file for unum functions
 */

typedef int bool;
#define false 0
#define true 1

//we're using int as the intermediate type when calculating the exponent
//added by me
#define EXP_SIZE 16
#define FRACTION_SIZE 16
//added by me
#define INTERMEDIATE_EXP_SIZE 16
#define INTERMEDIATE_FRACTION_SIZE 64

#define SINGLE_PREC_BIAS 127
#define SINGLE_PREC_F_SIZE 23
#define INT_SIZE 32

//colours
#define RESET "\033[0m"
#define KBLK "\x1B[30m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"

//unum type
typedef struct unum_s {
    bool sign;
    unsigned short exponent;
   // unsigned int exponent;
  // char exponent;
   unsigned int fraction;
    bool ubit;
    unsigned short e_size;
    unsigned short f_size;

  // unsigned short fraction;
   // char e_size;
  //  char f_size;
} unum_t;

typedef union {
    float f;

    struct {
        unsigned int fraction : 23;
        unsigned int exponent : 8;
        unsigned int sign : 1;
    } parts;
} double_cast;

typedef union {
    double f_lb;
    double f_rb;
    bool lb;
    bool rb;
    struct {
         unum_t left_bound;
         unum_t right_bound;
    } ubound_parts;
} ubFloat;


typedef struct ubound_s
{
    unum_t left_bound;
    unum_t right_bound;
    bool InexactFlag;
    bool uflag;
    bool vflag;
} ubound_t;

// global environment variables
int _g_esizesize;
int _g_fsizesize;
int _g_esizemax;
int _g_fsizemax;
int _g_fsizemaxcap;
int _g_utagsize;
int _g_maxubits;

int _g_maxexpval;
int _g_maxfracval;
int _g_smallnormalu_exp;

unum_t _g_ulpu;
unum_t _g_smallsubnormalu;
unum_t _g_smallnormalu;
unum_t _g_signbigu;
unum_t _g_posinfu;
unum_t _g_maxrealu;
unum_t _g_minrealu;
unum_t _g_maxrealuopen;
unum_t _g_minrealuopen;
unum_t _g_neginfu;
unum_t _g_negbigu;
unum_t _g_qNaNu;
unum_t _g_sNaNu;
unum_t _g_negopeninfu;
unum_t _g_posopeninfu;
unum_t _g_negzeroopen;
unum_t _g_zero;
unum_t _g_negzero;
unum_t _g_Posone;
unum_t _g_Negone;
unum_t _g_zeroopen;
unum_t _g_posinfopenu;
unum_t _g_smallsubnormaluopen;
unum_t _g_negsmallsubnormaluopen;
unum_t _g_utagmask;
float _g_maxreal;
float _g_smallnormal;
float _g_smallsubnormal;
int _g_maxexp;
int _g_minexp;
int _g_minnormalexp;

/*
 *Initializes a unum
 */
void unum_init(unum_t *x);

/*
 * Initialize unum environment and its global variables
 */
void set_env(int e_sizesize, int f_sizesize);

/*
 * print the integer values of the unum
 * (somewhat similar to uview of the prototype)
 */
void printu(unum_t u);

/*
 * print the integer bounds of the unum
 *
 */
void printub(ubound_t u);
float u2f(unum_t u);

float ub2f(const ubound_t *ub);

int ub2f_new( ubound_t *ub, double *f_lb, double *f_rb, bool *lb, bool *rb);

int ub2f_left_new(const ubound_t *ub,double *LB_left,double *LB_right);
int ub2f_right_new(const ubound_t *ub,double *RB_left,double *RB_right);



/*
 * Convert a ubound to single precision float value
 */
float ub2f_left(ubound_t* ub);
/*
 * Convert a ubound to single precision float value
 */

float ub2f_right (ubound_t* ub);

float ub2f_lb_right(ubound_t* ub);

float ub2f_rb_left(ubound_t* ub);


/*
 * Compare two unums for equality
 */
bool unum_compare(unum_t x, unum_t y);

/*
 * Converts a single precision float to a unum
 */
void x2u(float f, unum_t* u);

/*
 * print bits of an unsigned long long
 */
void print_bits(unsigned long long x, char* colour, unsigned short width);

/*
 * View the unum
 * (Similar to unumview of the prototype)
 */
void unumview(unum_t x);

/*
 * Multiply two unums
 */
void unum_mul(unum_t *result, unum_t *op1, unum_t *op2);

/*
* Unum Addition
*/

int unum_add(ubound_t *result,unum_t *x,unum_t *y);
/*
* Ubound Addition
*/

int ubound_add(ubound_t *result,ubound_t *x,ubound_t *y);
/*
 * Add two exact unums
 */

void ubound_addExact_left_bound(ubound_t *result_ub,  ubound_t* op1,  ubound_t* op2);


void ubound_addExact_right_bound(ubound_t *result_ub,  ubound_t* op1,  ubound_t* op2);
/*
 * Add two inexact unums - add the left ubounds separately
 */
void ubound_addInexact_left_bound(ubound_t *result_ub,  ubound_t *op1,  ubound_t *op2);
/*
 * Add two inexact unums - add the right ubounds separately
 */
void ubound_addInexact_right_bound(ubound_t *result_ub,  ubound_t *op1,  ubound_t *op2);



void ubound_subExact_left_bound(ubound_t *result_ub,  ubound_t* op1,  ubound_t* op2);

void ubound_subExact_right_bound(ubound_t *result_ub,  ubound_t* op1,  ubound_t* op2);
/*
 * Add two inexact unums - add the left ubounds separately
 */
void ubound_subInexact_left_bound(ubound_t *result_ub, ubound_t *op1,  ubound_t *op2);
/*
 * Add two inexact unums - add the right ubounds separately
 */
void ubound_subInexact_right_bound(ubound_t *result_ub, ubound_t *op1, ubound_t *op2);


/*
 *Initializes a ubound
 */
void ubound_init(ubound_t *ub);

void swap_bounds(float *lb,float *rb);

void swap_boundsUnum(unum_t *lb,unum_t *rb);


void swap_ubounds(ubound_t *lb,ubound_t *rb);

/*
 *Initializes a ubound from a single unum
 */
int unum2ubound(ubound_t *ub, unum_t *u);

/*
 *Initializes a ubound from a pair of unums
 */
int unumpair2ubound(ubound_t *ub, unum_t *u, unum_t *v);

//void get_unum_string_for_test(char* unum_string, unum_t x);

//void get_bit_string_for_test(char* buffer, int x, int width);

/*
  * Returns true if the unum is NaN, false otherwise
  */
 bool isNaN(unum_t u);

/*Returns true if the unum is negative or positive infinity,
false otherwise*/

 bool isInf(unum_t u);

 bool isPosInf(unum_t u) ;

 bool isNegInf(unum_t u);

 /*
* Returns true if the unum is NaN or negative or positive infinity, false otherwise
* Returns true if the unum is NaN or negative or positive infinity,
 * false otherwise
  */
 bool isNaNOrInf(unum_t u);

bool isPosopeninfu(unum_t u);

bool isPosOrNegZero(unum_t u);

bool isPosOrNegZeroExact(unum_t u);

bool isPosOrNegOne(unum_t u);

bool isPosOrNegInexactSmallsubnormal(unum_t u);

bool isPosOrNegInexactMaxreal(unum_t u);

#endif
