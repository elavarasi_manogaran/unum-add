#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "libunum.h"


void ubound_from_unum_init(ubound_t *ub, unum_t u) {

	  (*ub).left_bound = u;
      (*ub).right_bound = u;
      //If ubit is not set, then return and do exact addition
    if(!u.ubit)
        return;
    //If ubit is set for any one of the operands, then calculate the left and the right bounds separately
	else
	{

     //If any one of the operands are positive or negative inexact zero, then copy it to both left and right bound, add 1 to fraction and return
    if(isPosOrNegZeroInexact(u))
         {
            (*ub).left_bound = u;
            (*ub).right_bound = u;
            (*ub).right_bound.fraction+=1;

         return;
    }

        if(isNaN(u))
           {
              (*ub).left_bound = _g_qNaNu;
              (*ub).right_bound = _g_qNaNu;
              return;
           }

            if(isPosInf(u))
           {
            (*ub).left_bound = _g_posinfu;
            (*ub).right_bound = _g_posinfu;
            return;
           }

            if(isNegInf(u))
           {
            (*ub).left_bound = _g_neginfu;
            (*ub).right_bound = _g_neginfu;
            return;
           }



  //If the fraction is already maximum, then add 1 to the exponent and make the fraction 0
                if ((*ub).left_bound.fraction ==_g_maxfracval)
                        {
                            (*ub).right_bound.exponent++;
                            (*ub).right_bound.fraction= 0;
                            }
                //If fraction could be reduced, reduce 1 ULP to get the right bound result
                else
                   (*ub).right_bound.fraction++;
//Reset the ubit of the right bound to avoid landing in NaN

                 (*ub).right_bound.ubit = 0;
}
}

void set_env(int e_sizesize, int f_sizesize) {
    _g_esizesize = e_sizesize;
    _g_fsizesize = f_sizesize;
    _g_esizemax = pow(2, e_sizesize);
    _g_fsizemax = pow(2, f_sizesize);
    _g_maxexpval = pow(2, _g_esizemax) - 1;
   _g_maxfracval = pow(2, _g_fsizemax) - 1;
    _g_smallnormalu_exp = 1 - (pow(2, (_g_esizemax - 1)) - 1);
    _g_utagsize = 1 + e_sizesize + f_sizesize;
    _g_maxubits = 1 + _g_esizemax + _g_fsizemax + _g_utagsize;
    _g_ulpu = (unum_t) { .sign = 0, .exponent = 0, .fraction = 1, .ubit = 0, .e_size = 0, .f_size = 0 };
    _g_smallsubnormalu = (unum_t) { .sign = 0, .exponent = 0, .fraction = 1, .ubit = 0, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
    _g_smallnormalu = (unum_t) { .sign = 0, .exponent = 1, .fraction = 0, .ubit = 0, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
    _g_signbigu = (unum_t) { .sign = 0, .exponent = 0, .fraction = 0, .ubit = 0, .e_size = 0, .f_size = 0 };
    _g_posinfu = (unum_t) { .sign = 0, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 1), .ubit = 0, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
    _g_posinfopenu = (unum_t) { .sign = 0, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 1), .ubit = 1, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
    _g_maxrealu = (unum_t) { .sign = 0, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 2), .ubit = 0, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
    _g_minrealu = (unum_t) { .sign = 1, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 2), .ubit = 0, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
    _g_neginfu = (unum_t) { .sign = 1, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 1), .ubit = 0, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
    _g_negbigu = (unum_t) {.sign = 0, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 1), .e_size = _g_esizemax - 1, .f_size = _g_fsizemax - 1, .ubit = 1};
    _g_qNaNu = (unum_t) { .sign = 0, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 1), .e_size = ( _g_esizemax - 1), .f_size = (_g_fsizemax - 1), .ubit = 1 };
    _g_sNaNu = (unum_t) { .sign = 1, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 1), .e_size = ( _g_esizemax - 1), .f_size = (_g_fsizemax - 1), .ubit = 1 };

    _g_negopenzerou = (unum_t) { .sign = 1, .exponent = 0, .fraction = 0, .ubit = 1, .e_size = 0, .f_size = 0 };
    _g_zero = (unum_t) {0, 0, 0, 0, 0, 0};
    _g_Posone =  (unum_t) {0, 0, 1, 0, 0, 0};
    _g_Negone =  (unum_t) {1, 0, 1, 0, 0, 0};
    _g_zeroopen = (unum_t) {0, 0, 0, 1, 0, 0};

    _g_maxreal = pow(2, pow(2, (_g_esizemax - 1))) * (pow(2, _g_fsizemax) - 1) / pow(2, (_g_fsizemax - 1));
    _g_smallnormal = pow(2, (1 - ((pow(2, (_g_esizemax - 1)) - 1))));
    _g_smallsubnormal = pow(2, (2 - pow(2, (_g_esizemax - 1)) - _g_fsizemax));
    _g_maxexp = pow(2, (_g_esizemax - 1));
    _g_minexp = 1 - pow(2, (_g_esizemax - 1)) - _g_fsizemax;
    _g_minnormalexp = (1 - ((pow(2, (_g_esizemax - 1)) - 1)));

     if (_g_utagsize == 1)
        {
             _g_negopeninfu = (unum_t) { .sign = 1, .exponent = 1, .fraction = 0, .ubit = 1, .e_size = 0, .f_size = 0 };
             _g_minrealuopen = (unum_t) { .sign = 1, .exponent = 1, .fraction = 0, .ubit = 1, .e_size = 0, .f_size = 0 };
        }


    else
       {
           _g_negopeninfu = (unum_t) { .sign = 1, .exponent = 1, .fraction = 1, .ubit = 1, .e_size = 0, .f_size = 0 };
           _g_minrealuopen = (unum_t) { .sign = 1, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 2), .ubit = 1, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
       }

    if (_g_utagsize == 1)
    {
         _g_posopeninfu = (unum_t) { .sign = 0, .exponent = 1, .fraction = 0, .ubit = 1, .e_size = 0, .f_size = 0 };
         _g_maxrealuopen = (unum_t) { .sign = 0, .exponent = 1, .fraction = 0, .ubit = 1, .e_size = 0, .f_size = 0 };

    }

    else
    {
       _g_posopeninfu = (unum_t) { .sign = 0, .exponent = 1, .fraction = 1, .ubit = 1, .e_size = 0, .f_size = 0 };
       _g_maxrealuopen = (unum_t) { .sign = 0, .exponent = (pow(2, _g_esizemax) - 1), .fraction = (pow(2, _g_fsizemax) - 2), .ubit = 1, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1) };
    }

}

//Function to compute the float of the left bound

float ub2f_left(const ubound_t *ub) {

    float f, fraction;
    int exponent;

    if(isPosInf((*ub).left_bound))
       {
        return INFINITY;
       }

     if(isNegInf((*ub).left_bound)){

         return -INFINITY;
     }


    if(isNaN((*ub).left_bound)){

         return NAN;
    }

     if(isPosOrNegInexactMaxreal((*ub).left_bound)){


               if((*ub).left_bound.sign)
                return - _g_maxreal;
               else
                return _g_maxreal;

    }


   if (!(*ub).left_bound.exponent) {

        exponent = 1 - (pow(2, (*ub).left_bound.e_size) - 1);
        fraction = (*ub).left_bound.fraction / pow(2, ((*ub).left_bound.f_size + 1));
    } else {

        exponent = (*ub).left_bound.exponent - (pow(2, (*ub).left_bound.e_size) - 1);
        fraction = 1 + (*ub).left_bound.fraction / pow(2, ((*ub).left_bound.f_size + 1));
    }

    f = pow(-1, (*ub).left_bound.sign) * pow(2, exponent) * fraction;

    return f;

}

//Function to compute the float of the right bound

float ub2f_right (const ubound_t *ub) {
    float f, fraction;
    int exponent;


    if(isPosInf((*ub).right_bound)){

     return INFINITY;
    }

    if(isNegInf((*ub).right_bound)){

     return -INFINITY;

    }

    if(isNaN((*ub).right_bound)){

     return NAN;
    }

    if(isPosOrNegInexactMaxreal((*ub).right_bound)){

      if((*ub).right_bound.sign)
         return -INFINITY;
      else
         return INFINITY;
    }

    if (!(*ub).right_bound.exponent) {

        //If the (*ub)it is set for the left bound of the result, then add 1 to the fraction to get the float value
            if((*ub).left_bound.ubit){

                fraction = (1 + (*ub).right_bound.fraction) / pow(2, ((*ub).right_bound.f_size + 1));

           }
            else{

             fraction = (*ub).right_bound.fraction / pow(2, ((*ub).right_bound.f_size + 1));
            }

            exponent = 1 - (pow(2, (*ub).right_bound.e_size) - 1);
            }

    else {


        if((*ub).left_bound.ubit)
        {

             //If the ubit is set for the left bound of the result, then add 1 to the fraction to get the float value
            fraction = 1 + (1+ (*ub).right_bound.fraction) / pow(2, ((*ub).right_bound.f_size + 1));

             }
        else
        {

           fraction = 1 + (*ub).right_bound.fraction / pow(2, ((*ub).right_bound.f_size + 1));
        }

        exponent = (*ub).right_bound.exponent - (pow(2, (*ub).right_bound.e_size) - 1);
    }

    f = pow(-1, (*ub).right_bound.sign) * pow(2, exponent) * fraction;



   return f;
}

void swap_bounds(float *lb,float *rb)
{
 float temp;
 temp = *rb;
 *rb = *lb;
 *lb = temp;
 return;
 }



/**
 * Returns true if two unums are equal and false otherwise
 */
bool unum_compare(unum_t x, unum_t y) {
    if (x.sign != y.sign)
        return false;

    if (x.exponent != y.exponent)
        return false;

    if (x.fraction != y.fraction)
        return false;

    if (x.ubit != y.ubit)
        return false;

    if ((x.e_size ^ y.e_size) != 0)
        return false;

    if ((x.f_size ^ y.f_size) != 0)
        return false;
    else
        return true;
}

void x2u(float f, unum_t* u) {
   double_cast d1;
    unsigned int fraction_bit_length, intermediate_exp, trailing_zeros, intermediate_frac;
    int final_exponent, exp_scale;

    //check for NaN, plus infinity, minus infinity, zero
    if (f != f)
        *u = _g_qNaNu;
    if (f == INFINITY)
        *u = _g_posinfu;
    if (f == -INFINITY)
        *u = _g_neginfu;
    if (f == 0)
        *u = _g_zero;

    //maxrealu
    if (fabsf(f) > _g_maxreal) {
		*u = _g_maxrealu;
		(*u).ubit = 1;
		(*u).sign = d1.parts.sign;
		return;
	}

    //smallsubnormalu
    if (fabsf(f) < _g_smallsubnormal) {
		*u = _g_smallsubnormalu;
		(*u).ubit = 1;
		(*u).sign = d1.parts.sign;
		return;
	}

    d1.f = f;
    (*u).sign = d1.parts.sign;
    (*u).ubit = 0;

    //smallnormalu
    if (fabsf(f) < _g_smallnormal) {
		(*u).exponent = 0;
		(*u).e_size = (_g_esizemax - 1);
		intermediate_frac = d1.parts.fraction;

		if (d1.parts.exponent != 0) {
			//unum exponent - float exponent + 1 (adding one to compensate for hidden bit)
			exp_scale = (1 - (pow(2, (*u).e_size) - 1)) - (d1.parts.exponent - SINGLE_PREC_BIAS + 1);
			intermediate_frac |= (1 << SINGLE_PREC_F_SIZE);	//hidden 1
			fraction_bit_length = SINGLE_PREC_F_SIZE + 1;
		} else {
			exp_scale = (1 - (pow(2, (*u).e_size) - 1)) - (1 - SINGLE_PREC_BIAS);
			fraction_bit_length = SINGLE_PREC_F_SIZE;
		}

        if(intermediate_frac == 0) {
			(*u).fraction = 0;
            (*u).f_size = 0;
            return;
        }

		//set ubit before clipping
		if(intermediate_frac != 0 && ((fraction_bit_length - __builtin_ctz(intermediate_frac)) > _g_fsizemax))
			(*u).ubit = 1;

		if(exp_scale < 0) {
			intermediate_frac <<= abs(exp_scale);
			fraction_bit_length -= exp_scale;
		} else {
			//because the fraction size of single precision float(23) is
			//greater than that of the unum type(16), we can do this
			intermediate_frac >>= abs(exp_scale);
		}

		if(fraction_bit_length > _g_fsizemax) {
			(*u).fraction = intermediate_frac >> (fraction_bit_length - _g_fsizemax);
			(*u).f_size = _g_fsizemax - 1;
		} else {
			(*u).fraction = intermediate_frac;
			(*u).f_size = fraction_bit_length - 1;
		}

		//clip off any trailing zeros in the fraction
		if((*u).fraction != 0 && __builtin_ctz((*u).fraction) && (*u).ubit == 0) {
			(*u).f_size -= __builtin_ctz((*u).fraction);
			(*u).fraction >>= __builtin_ctz((*u).fraction);
		}

		return;
	}

    //normal numbers
    //http://stackoverflow.com/questions/15685181/how-to-get-the-sign-mantissa-and-exponent-of-a-floating-point-number
    if (d1.parts.exponent != 0) {

        //fraction
        if (d1.parts.fraction == 0) {
            if(d1.parts.exponent != 127) {
                (*u).f_size = 0;
                (*u).fraction = 0;
            } else {
                //unum for 1 is subnormal, therefore handled as a special case
                (*u).f_size = 0;
                (*u).fraction = 1;
                (*u).exponent = 0;
                (*u).e_size = 0;
                return;
            }
        } else {
            trailing_zeros = __builtin_ctz(d1.parts.fraction);
            fraction_bit_length = SINGLE_PREC_F_SIZE - trailing_zeros;

            if (fraction_bit_length > _g_fsizemax) {
                //set ubit
                (*u).ubit = 1;

                //shift until it fits _g_fsizemax bits
                d1.parts.fraction = d1.parts.fraction >> (SINGLE_PREC_F_SIZE - _g_fsizemax);
                (*u).fraction = d1.parts.fraction;
                (*u).f_size = _g_fsizemax - 1;
            } else {
                (*u).f_size = fraction_bit_length - 1;
                (*u).fraction = d1.parts.fraction >> trailing_zeros;
            }
        }

        //exponent
        final_exponent = d1.parts.exponent - 127;

    } else {
        //zero - a special case?
        if (d1.parts.fraction == 0) {
            *u = _g_zero;
            return;
        } else {
            trailing_zeros = __builtin_ctz(d1.parts.fraction);
            fraction_bit_length = SINGLE_PREC_F_SIZE - trailing_zeros;

            final_exponent = -126;
            exp_scale = __builtin_clz(d1.parts.fraction) - (INTERMEDIATE_EXP_SIZE - SINGLE_PREC_F_SIZE) + 1; //the number of leading zeros plus one (for the hidden bit))
            final_exponent = final_exponent - exp_scale;
            fraction_bit_length = fraction_bit_length - exp_scale;

            //set the most significant bit to zero (the hiddent bit)
            d1.parts.fraction &= ~(1 << (INTERMEDIATE_EXP_SIZE - __builtin_clz(d1.parts.fraction) - 1));

            if(d1.parts.fraction == 0) {
                (*u).f_size = 0;
                (*u).fraction = 0;
                if ((SINGLE_PREC_F_SIZE - trailing_zeros) > _g_fsizemax)
                    (*u).ubit = 1;
            } else if (fraction_bit_length > _g_fsizemax) {
                //set ubit
                (*u).ubit = 1;
                //shift until it fits 16 bits
                (*u).fraction = d1.parts.fraction >> (SINGLE_PREC_F_SIZE - exp_scale - _g_fsizemax);
                (*u).f_size = 15;
            } else {
                (*u).f_size = fraction_bit_length - 1;
                (*u).fraction = d1.parts.fraction >> trailing_zeros;
            }
        }
    }

    //set exponent and exponent size based on the final exponent value
    if (final_exponent == 1) {
        (*u).e_size = 0;
        (*u).exponent = 1;
    } else {
        intermediate_exp = abs(final_exponent - 1) + 1;
        if((intermediate_exp != 0) && ((intermediate_exp & (~intermediate_exp + 1)) == intermediate_exp)) {
            (*u).e_size = INTERMEDIATE_EXP_SIZE - __builtin_clz(intermediate_exp) - 1;
        } else {
            (*u).e_size = INTERMEDIATE_EXP_SIZE - __builtin_clz(intermediate_exp);
        }

        (*u).exponent = final_exponent + (pow(2, ((*u).e_size)) - 1);
    }
    return;
}


void print_bits(unsigned long long x, char* colour, unsigned short width) {
    printf("%s", colour);
    int size, i, leading_zeros = 0;
    unsigned long long bitmask;

    if (width == 0)
        return;

    if (x == 0) {
        for (i = 0; i < width; i++) {
            printf("0");
        }
        printf(RESET);
        return;
    } else {
        size = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(x) - 1;
        leading_zeros = __builtin_clzll(x) - (INTERMEDIATE_FRACTION_SIZE - width);
        for (i = 0; i < leading_zeros; i++) {
            printf("0");
        }
    }

    for (i = size; i >= 0; i--) {
        bitmask = 1ULL << i;
        //printf("i=%d %llu\n",i,bitmask);
        if (x & bitmask)
            printf("1");
        else
            printf("0");
    }
    printf(RESET);
    return;
}

void printu(unum_t u) {
    printf("%d\n", u.sign);
    printf("%d\n", u.exponent);
    printf("%d\n", u.fraction);
    printf("%d\n", u.ubit);
    printf("%d\n", u.e_size);
    printf("%d\n", u.f_size);
}

void printub(ubound_t u) {

 	    printf("%d\n", u.left_bound.sign);
        printf("%d\n", u.left_bound.exponent);
        printf("%d\n", u.left_bound.fraction);
        printf("%d\n", u.left_bound.ubit);
        printf("%d\n", u.left_bound.e_size);
        printf("%d\n", u.left_bound.f_size);

        printf("%d\n", u.right_bound.sign);
        printf("%d\n", u.right_bound.exponent);
        printf("%d\n", u.right_bound.fraction);
        printf("%d\n", u.right_bound.ubit);
        printf("%d\n", u.right_bound.e_size);
        printf("%d\n", u.right_bound.f_size);

}

bool isNaN(unum_t u) {
     return (u.exponent == _g_maxexpval && u.fraction == _g_maxfracval && u.ubit);
 }


bool isInf(unum_t u) {
     return (u.exponent == _g_maxexpval && u.fraction == _g_maxfracval && !u.ubit);
 }


bool isNegInf(unum_t u) {
     return (u.sign && u.exponent == _g_maxexpval && u.fraction == _g_maxfracval && !u.ubit);
 }


bool isPosInf(unum_t u) {
     return (!u.sign && u.exponent == _g_maxexpval && u.fraction == _g_maxfracval && !u.ubit);
 }

bool isNaNOrInf(unum_t u)
{
    return (u.exponent == _g_maxexpval && u.fraction == _g_maxfracval);
}

bool isPosOrNegInexactMaxreal(unum_t u)
{
    return (u.exponent == _g_maxexpval && u.fraction == (_g_maxfracval - 1) && u.ubit);
}

bool isPosOrNegMaxreal(unum_t u)
{
    return (u.exponent == _g_maxexpval && u.fraction == (_g_maxfracval - 1));
}

bool isPosOrNegZero(unum_t u)
{
    return !(u.exponent | u.fraction | u.ubit);
}

bool isPosOrNegZeroInexact(unum_t u)
{
    return (!u.exponent && !u.fraction && u.ubit);
}


bool isPosopeninfu_0_0(unum_t u)
{
    return (u.exponent == 1 && u.fraction == 0 && u.ubit ==1 && u.e_size == 0 && u.f_size == 0 );
}

bool isPosopeninfu(unum_t u)
{
    return (u.exponent == 1 && u.fraction == 1 && u.ubit ==1 && u.e_size == 0 && u.f_size == 0 );
}

bool isPosOrNegOne(unum_t u)
{
    return (!(u.fraction | u.ubit ) && (u.exponent == 1));
}
