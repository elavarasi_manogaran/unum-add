/*Tests for unum addition (0,0)*/
#include "unum_add_test.h"
#include "libunum.h"


void test_unum_add_0_0()
{
    unum_t unum_x, unum_y,unum_y_left, unum_y_right;
    ubound_t op1,op2,op3,op4,result,result2,result1;
	double left_bound,right_bound;
	bool nan =0, left_open, right_open;

  //set environment
    set_env(0, 0);

//Testcases including ubound pairs


//Testcase 1: -1 + [-2,-0)
//Ans: (-Inf,-1)
    unum_x	=	(unum_t)	{1,0,1,0,0,0};
    unum_y_left = (unum_t)	{1,1,0,0,0,0};
    unum_y_right = (unum_t)	{1,0,0,1,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
      //  printf("left_bound=%lf,right_bound=%lf,left_open=%d,right_open=%d",left_bound,right_bound,left_open,right_open);

    assert((left_bound==	-INFINITY	)&&(right_bound==	-1	));
    assert((left_open==	1	)&&(right_open==	1	));


//Testcase 2: -1 + [-2,-1]
//Ans: (-Inf,-2]
    unum_x	=	(unum_t)	{1,0,1,0,0,0};
    unum_y_left = (unum_t)	{1,1,0,0,0,0};
    unum_y_right = (unum_t)	{1,0,1,0,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
   // printf("left_bound=%lf,right_bound=%lf,left_open=%d,right_open=%d",left_bound,right_bound,left_open,right_open);
    assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    assert((left_open==	1	)&&(right_open==	0	));


//Testcase 3: -1 + [-2,-1)
//Ans: (-Inf,-2)
    unum_x	=	(unum_t)	{1,0,1,0,0,0};
    unum_y_left = (unum_t)	{1,1,0,0,0,0};
    unum_y_right = (unum_t)	{1,0,1,1,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    assert((left_open==	1	)&&(right_open==	1	));



//Testcase 4: 1 +  (0,2]
//Ans: (1,Inf)
    unum_x	=	(unum_t)	{0,0,1,0,0,0};
    unum_y_left = (unum_t)	{0,0,0,1,0,0};
    unum_y_right = (unum_t)	{0,1,0,0,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	1	)&&(right_bound==	INFINITY	));
    assert((left_open==	1	)&&(right_open==	1	));

//Testcase 5: 1 + [1,2]
//Ans: [2,Inf)
    unum_x	=	(unum_t)	{0,0,1,0,0,0};
    unum_y_left = (unum_t)	{0,0,1,0,0,0};
    unum_y_right = (unum_t)	{0,1,0,0,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    assert((left_open==	0	)&&(right_open==	1	));
    //  printf("left_bound=%lf,right_bound=%lf,left_open=%d,right_open=%d",left_bound,right_bound,left_open,right_open);


//Testcase 6: 1 + (1,2]
//Ans: (2,INF)
    unum_x	=	(unum_t)	{0,0,1,0,0,0};
    unum_y_left = (unum_t)	{0,0,1,1,0,0};
    unum_y_right = (unum_t)	{0,1,0,0,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    assert((left_open==	1	)&&(right_open==	1	));

//Only ubound testcases

    unum_x	=	(unum_t)	{0,0,0,0,0,0};
    unum_y	=	(unum_t)	{0,0,1,0,0,0};
    unum2ubound(&op1,&unum_x);
    unum2ubound(&op2,&unum_y);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	1	)&&(right_bound==	1	));
    assert((left_open==	0	)&&(right_open==	0	));

//subtraction

//Testcase 1: 1 + [-2,-0)
//Ans: (-Inf,-1)
    unum_x	=	(unum_t)	{0,0,1,0,0,0};
    unum_y_left = (unum_t)	{1,1,0,0,0,0};
    unum_y_right = (unum_t)	{1,0,0,1,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);

    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
      //  printf("left_bound=%lf,right_bound=%lf,left_open=%d,right_open=%d",left_bound,right_bound,left_open,right_open);

    assert((left_bound==	-1	)&&(right_bound==	1	));
    assert((left_open==	0	)&&(right_open==	1	));


//Testcase 2: -1 + [-2,-1]
//Ans: (-Inf,-2]
    unum_x	=	(unum_t)	{0,0,1,0,0,0};
    unum_y_left = (unum_t)	{1,1,0,0,0,0};
    unum_y_right = (unum_t)	{1,0,1,0,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
   // printf("left_bound=%lf,right_bound=%lf,left_open=%d,right_open=%d",left_bound,right_bound,left_open,right_open);
    assert((left_bound==	-1	)&&(right_bound==	0	));
    assert((left_open==	0	)&&(right_open==	0	));


//Testcase 3: -1 + [-2,-1)
//Ans: (-Inf,-2)
    unum_x	=	(unum_t)	{0,0,1,0,0,0};
    unum_y_left = (unum_t)	{1,1,0,0,0,0};
    unum_y_right = (unum_t)	{1,0,1,1,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	-1	)&&(right_bound==	0	));
    assert((left_open==	0	)&&(right_open==	1	));



//Testcase 4: 1 +  (0,2]
//Ans: (1,Inf)
    unum_x	=	(unum_t)	{1,0,1,0,0,0};
    unum_y_left = (unum_t)	{0,0,0,1,0,0};
    unum_y_right = (unum_t)	{0,1,0,0,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	-1	)&&(right_bound==	1	));
    assert((left_open==	1	)&&(right_open==	0	));

//Testcase 5: 1 + [1,2]
//Ans: [2,Inf)
    unum_x	=	(unum_t)	{1,0,1,0,0,0};
    unum_y_left = (unum_t)	{0,0,1,0,0,0};
    unum_y_right = (unum_t)	{0,1,0,0,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	0	)&&(right_bound==	1	));
    assert((left_open==	0	)&&(right_open==	0	));
    //  printf("left_bound=%lf,right_bound=%lf,left_open=%d,right_open=%d",left_bound,right_bound,left_open,right_open);


//Testcase 6: 1 + (1,2]
//Ans: (2,INF)
    unum_x	=	(unum_t)	{1,0,1,0,0,0};
    unum_y_left = (unum_t)	{0,0,1,1,0,0};
    unum_y_right = (unum_t)	{0,1,0,0,0,0};
    unum2ubound(&op1,&unum_x);
    unumpair2ubound(&op2,&unum_y_left,&unum_y_right);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	0	)&&(right_bound==	1	));
    assert((left_open==	1	)&&(right_open==	0	));

//Only ubound testcases

    unum_x	=	(unum_t)	{0,0,0,0,0,0};
    unum_y	=	(unum_t)	{0,0,1,0,0,0};
    unum2ubound(&op1,&unum_x);
    unum2ubound(&op2,&unum_y);
    ubound_add(&result,&op1,&op2);
    ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);
    assert((left_bound==	1	)&&(right_bound==	1	));
    assert((left_open==	0	)&&(right_open==	0	));


    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	2	));

    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	2	));
    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));


    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);



    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open); assert((left_bound==	0	)&&(right_bound==	1	));

    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	2	));

    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	2	));

    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	1	));


    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	2	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	2	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);

    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	2	));

    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open); 	assert((left_bound==	2	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);

    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	2	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	2	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);

    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);


    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);  assert((left_bound==	-2	)&&(right_bound==	0	));



    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-2	)&&(right_bound==	-1	));


    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-1	));
    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-2	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-2	)&&(right_bound==	-2	));

    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));

    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);

    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));

    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	 assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));

    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);

    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);

   //subtraction

    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	0	));

    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-2	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-2	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{0,0,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);


    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open); 	assert((left_bound==	-1	)&&(right_bound==	1	));



    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open); 	assert((left_bound==	-1	)&&(right_bound==	0	));



    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-2	)&&(right_bound==	0	));


    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-2	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{0,0,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);




    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	1	));



    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	0	));

    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	-1	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-1	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);



    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	2	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	1	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	1	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	0	));

    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{0,0,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);



    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	2	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	1	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	1	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);




    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{0,1,0,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);




    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,0,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);





    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{0,1,1,1,0,0}	;	unum_y	=	(unum_t)	{1,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	0	));

    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	1	));
    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	1	));
    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	2	));

    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	2	));


    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{1,0,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);






    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	0	));


    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	1	));

    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	1	));

    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	1	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{1,0,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);





    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-2	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-2	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	1	));

    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	1	));

    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{1,0,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);




    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-2	)&&(right_bound==	-2	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-2	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-1	)&&(right_bound==	0	));

    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	0	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{1,1,0,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);




    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-2	));

    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-1	));

    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	0	));
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	0	));

    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	INFINITY	));

    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	INFINITY	)&&(right_bound==	INFINITY	));
    unum_x	=	(unum_t)	{1,1,0,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);



    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	assert((left_bound==	-INFINITY	)&&(right_bound==	-INFINITY	));
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,0,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);



    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,0,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,0,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,0,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);
    unum_x	=	(unum_t)	{1,1,1,1,0,0}	;	unum_y	=	(unum_t)	{0,1,1,1,0,0}	;unum2ubound(&op1,&unum_x);	unum2ubound(&op2,&unum_y)	;ubound_add(&result,&op1,&op2);		ub2f_new(&result,&left_bound,&right_bound,&left_open,&right_open);	if (isnan(left_bound) && isnan(right_bound))nan=1;assert(nan== 1	);

/*

   printf("\n unum add result is\n");
   printf("\n Left bound: %5.13f\n",left_bound);
   printf("\n");
   printu(result.left_bound);
   printf("\n Right bound: %5.13f\n",right_bound);
   printf("\n");
   printu(result.right_bound);

*/

printf("\n All plusu tests for 0_0 passed (including subtraction :-)\n");
}


